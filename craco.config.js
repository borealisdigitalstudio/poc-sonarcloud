const jestConfig = {
  coverageReporters: [
    'json',
    'lcov',
    'text',
    'clover',
    'cobertura'
  ],
};

module.exports = {
  jest: {
    configure: jestConfig,
  }
}